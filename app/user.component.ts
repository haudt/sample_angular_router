import { Component, OnInit } from '@angular/core';
import {User} from './user';
import {UserService} from './user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'my-user',
    template: `<ul>
    	<li *ngFor = "let user of users" (click) = "onSelect(user)" >
    		<span> {{user.id}}</span>
    		{{user.name}}
    	</li>
    </ul>
   <button (click)="gotoDetail()">View</button>
    `
})
export class UserComponent implements OnInit{
	users : User[];
	selectedUser : User;
	onSelect(user: User): void {
		this.selectedUser = user;
	}
	constructor(private userService: UserService,private router: Router,) {

	}
	getUsers(): void {
	this.userService.getUsers().then(users => this.users = users);
	}

	ngOnInit():void {
		this.getUsers();
	}
	  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedUser.id]);
  }
 }
