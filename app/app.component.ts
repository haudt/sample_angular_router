import {Component} from '@angular/core';

@Component ({
	selector: 'my-app',
	template: `<h1>{{title}}</h1>
	<a routerLink = "/dashboard"> Dashboard</a>
     <a routerLink="/user">List Users</a>

     <router-outlet></router-outlet>`
})
export class AppComponent {
  title = 'Welcom to angular2';
}