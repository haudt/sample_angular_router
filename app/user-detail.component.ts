import 'rxjs/add/operator/switchMap';
import { Component,OnInit} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';
import { User } from './user';
import {UserService} from './user.service';

@Component({
  
  selector: 'my-user-detail',
  template: `<div *ngIf="user">
      <h2>{{user.name}} details!</h2>
      <div><label>id: </label>{{user.id}}</div>
      <div>
        <label>name: </label>
        <input [(ngModel)]="user.name" placeholder="name"/>
      </div>
    </div>
    <button (click)="goBack()">Back</button>
    `
})
export class UserDetailComponent {
  user: User;

   constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.userService.getUser(+params['id']))
      .subscribe(user => this.user = user);
  }

  goBack(): void {
    this.location.back();
  }
}
